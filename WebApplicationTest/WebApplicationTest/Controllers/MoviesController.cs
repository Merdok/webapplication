﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationTest.Models;

namespace WebApplicationTest.Controllers
{
    public class MoviesController : Controller
    {

        private MoviesContext db = new MoviesContext();

        // GET: /Movies/
        public ActionResult Index(string name = "", string genre = "", string actor = "", int page = 1)
        {
            try
            {
                //parse string for separating on name and surname
                var array = ParseString(actor);
                var nameActor = array[0];
                var surnameActor = array[1];

                var films = GetAllFilms(genre, name, nameActor, surnameActor);

                if(isListEmpty(films.ToList()))
                {
                    ViewBag.Message = "Ничего не найдено";
                }

                //main query, to get films for 1 page
                var query = AmountFilmsOnOnePage(films, page);

                var filter = new MoviesFilter
                {
                    indexViewModel = query,
                    Actor = actor,
                    Name = name,
                    Genres = new SelectList(new List<string>()
                    {
                        "Все",
                        "Детектив",
                        "Боевик",
                        "Драма",
                        "Мелодрама",
                        "Комедия",
                        "Приключения",
                        "Фантастика",
                        "Триллер",
                        "Криминал",
                        "Наука",
                        "Ужасы",
                        "Семейный",
                        "Биография"
                    }),
                    selectedGenre = genre
                };

                return View(filter);

            }
            catch (Exception e)
            {
                ViewBag.Error = e.ToString();
                Console.Write(e.ToString());
                return View(e);
            }
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return HttpNotFound();
            }

            var film = GetInfoAboutFilm(id);
            var actors = GetActorList(id);
            if(!film.Any() || !actors.Any())
            {
                return HttpNotFound();
            }

            var detailsMovie = new DetailsMovie
            {
                Film = film.FirstOrDefault(),
                Actors = actors
            };

            return View(detailsMovie);
        }

        public List<Film> GetInfoAboutFilm(int? id)
        {
            
            var query = db.Films.Where(f => f.FilmId == id).ToList();
            return query;
        }

        public List<Actor> GetActorList(int? id)
        {
            var query = ((from actors in db.Actors
                          join filmings in db.Filmings on actors.ActorId equals filmings.ActorId
                          where filmings.FilmId == id
                          select actors).Distinct()).ToList();
            return query;
        }

        public IQueryable<Film> GetAllFilms(string genre, string name, string nameActor, string surnameActor)
        {
            if (genre == "Все")
                genre = "";

            var query = ((from filmings in db.Filmings
                          join films in db.Films on filmings.FilmId equals films.FilmId
                          join actors in db.Actors on filmings.ActorId equals actors.ActorId
                          where films.Genre.Contains(genre) && films.Name.Contains(name) && (actors.LastName.Contains(nameActor)
                          || actors.LastName.Contains(surnameActor) || actors.FirstName.Contains(nameActor) || actors.FirstName.Contains(surnameActor))
                          select films).Distinct()).OrderBy(films => films.FilmId);

            return query;
        }

        public bool isListEmpty(List<Film> films)
        {
            if (!films.Any())
            {
                return true;
            }
            return false;
        }

        public String[] ParseString(string actor)
        {
            string[] initials = { "", "" };
            //if string contains space, necessary to parse (name, surname)
            if (actor.Contains(" "))
            {
                var array = actor.Split(' ');
                initials[0] = array[0];
                initials[1] = array[1];
                return initials;
            }

            initials[0] = initials[1] = actor;
            return initials;
        }

        public IndexViewModel AmountFilmsOnOnePage(IQueryable<Film> films, int currentPage)
        {
            int pageSize = 6;
            var pageInfo = new PageInfo { PageNumber = currentPage, PageSize = pageSize, TotalItems = films.Count() };
            var indexViewModel = new IndexViewModel { PageInfo = pageInfo, Films = films.Skip((currentPage - 1) * pageSize).Take(pageSize) };
            return indexViewModel;
        }
	}
}
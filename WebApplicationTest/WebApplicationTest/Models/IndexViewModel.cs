﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationTest.Models
{
    public class IndexViewModel
    {
        public IEnumerable<Film> Films { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}
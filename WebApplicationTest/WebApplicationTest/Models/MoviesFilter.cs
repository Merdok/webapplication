﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplicationTest.Models
{
    public class MoviesFilter
    {
        public IndexViewModel indexViewModel { get; set; }
        public SelectList Genres { get; set; }
        public string Name { get; set; }
        public string Actor { get; set; }
        public string selectedGenre { get; set; }
    }
}
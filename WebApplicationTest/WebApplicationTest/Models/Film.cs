﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplicationTest.Models
{
    public class Film
    {
        public int FilmId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public int Duration { get; set; }
        [Display(Name = "Продюссер: ")]
        public string Director { get; set; }
        public float Price { get; set; }
        [Display(Name = "Премьера: ")]
        public DateTime PremiereDate { get; set; }
        public string ImagePath { get; set; }
        public ICollection<Filming> Filmings { get; set; }
        public ICollection<Actor> Actors { get; set; } 
    }
}
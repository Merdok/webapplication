﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationTest.Models
{
    public class Filming
    {
        public int FilmingId { get; set; }
        public int ActorId { get; set; }
        public int FilmId { get; set; }
        public int? Salary { get; set; }
        public Actor actor { get; set; }
        public Film film { get; set; }
    }
}